import '../component/search-bar.js';
import '../component/movie-list';
import config from '../data/data-config.js';
import DataSource from '../data/data-source.js';

const main = () => {
    const searchElement = document.querySelector("search-bar");
    const movieListElement = document.querySelector("movie-list");
    const menuBarElement = document.querySelector("menu-bar");
    const menuBottomElement = document.querySelector("menu-bottom");

    const onButtonSearchClicked = () => {
        DataSource.searchMovie(searchElement.value)
            .then(renderResult)
            .catch(fallbackResult);
    }

    const getMovieList = config => {
        DataSource.getMovieList(config)
            .then(renderResult)
            .catch(fallbackResult);
    };

    const renderResult = results => {
        movieListElement.movies = results;
    };

    const fallbackResult = message => {
        movieListElement.renderError(message);
    };

    const onMenu = () => {
        const category = menuBarElement.value;
        switch (category) {
            case 'nowPlaying': getMovieList(config.nowPlaying); break;
            case 'upcoming': getMovieList(config.upcoming); break;
            case 'popular': getMovieList(config.popular); break;
        }
    }

    const onMenuBottom = () => {
        const category = menuBottomElement.value;
        switch (category) {
            case 'nowPlaying': getMovieList(config.nowPlaying); break;
            case 'upcoming': getMovieList(config.upcoming); break;
            case 'popular': getMovieList(config.popular); break;
        }
    }

    menuBarElement.clickEvent = onMenu;
    menuBottomElement.clickEvent = onMenuBottom;
    searchElement.clickEvent = onButtonSearchClicked;
    getMovieList(config.nowPlaying);
};

export default main;