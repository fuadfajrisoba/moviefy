import config from './data-config';
class DataSource {

    static getMovieList(config) {
        return fetch(config)
            .then(response => response.json())
            .then(responseJson => {
                if (responseJson.results) {
                    return Promise.resolve(responseJson.results);
                }
            })
    }

    static searchMovie(keyword) {
        return fetch(`${config.search}${keyword}`)
            .then(response => response.json())
            .then(responseJson => {
                if (responseJson.results && responseJson.total_results !== 0) {
                    return Promise.resolve(responseJson.results);
                } else {
                    return Promise.reject(`${keyword} is not found.`)
                }
            })
    }
}

export default DataSource;