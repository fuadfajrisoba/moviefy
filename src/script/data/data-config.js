const baseUrl = "https://api.themoviedb.org/3/";
const apiKey = "4257396b2038effbaff8c0f2c8b9cac6";

const config = {
    nowPlaying : `${baseUrl}movie/now_playing?api_key=${apiKey}`,
    upcoming : `${baseUrl}movie/upcoming?api_key=${apiKey}`,
    popular : `${baseUrl}movie/popular?api_key=${apiKey}`,
    search : `${baseUrl}search/movie?api_key=${apiKey}&query=`
}

export default config;