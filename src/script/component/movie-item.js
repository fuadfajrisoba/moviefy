class MovieItem extends HTMLElement {
    set movie(movie) {
        this._movie = movie;
        this.render();
    }

    render() {
        this.className = "col-lg-2 col-md-3 col-sm-6 mb-4"
        this.innerHTML = `
            <style>
            * {
                margin: 0;
                padding: 0;
                box-sizing: border-box;
            }

            .list-np img {
                box-shadow: 0 4px 8px hsla(357, 92%, 47%, 0.432);
            }
            
            .list-np:hover img {
                opacity: .3;
            }
            
            .detail {
                background-color: #DB0000;
                border-radius: .25rem;
                transition: .5s ease;
                opacity: 0;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);
            }
            
            .list-np:hover .detail {
                opacity: 1;
            }

            .modal-content {
                background-color: #121212;
            }

            .btn-detail {
                background-color: #E50914;
            }
            </style>
            <div class="list-np card bg-transparent">
                <img class="card-img-top rounded-3" src="https://image.tmdb.org/t/p/w185${this._movie.poster_path}" alt="${this._movie.title} Poster">
                <div class="detail">
                    <button href="#" type="button" id="btnDetail" class="btn btn-link text-white text-decoration-none" data-bs-toggle="modal" data-bs-target="#showDetail${this._movie.id}">Detail</button>
                </div>
            </div>
            
            <div class="modal fade" id="showDetail${this._movie.id}" tabindex="-1" aria-labelledby="movieTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="movieTitle">${this._movie.title}</h5>
                            <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6">
                                    <p><b>Release Date</b><br>
                                    ${this._movie.release_date}</p>

                                    <p><b>Popularity</b><br>
                                    ${this._movie.popularity}</p>

                                    <p><b>Rate</b><br>
                                    &#9733; ${this._movie.vote_average}</p>

                                    <p><b>Language</b><br>
                                    ${this._movie.original_language}</p>
                                </div>

                                <div class="col-6">
                                    <img src="https://image.tmdb.org/t/p/w185${this._movie.poster_path}" class="rounded-3 float-end" width="160px" alt="${this._movie.title} Poster">
                                </div>
                            </div>

                            <p><b>Overview</b><br>
                            ${this._movie.overview}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-detail text-white" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
           `;
    }
}

customElements.define("movie-item", MovieItem);