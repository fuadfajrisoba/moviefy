class FooterBar extends HTMLElement {
    connectedCallback(){
        this.render();
    }
  
    render() {
        this.innerHTML = `
        <footer>
            <p class="m-0 pt-4 text-white text-center">© 2021 Fuad Fajri Soba. All right reserved</p>
        </footer>
        `;
    }
}

customElements.define("footer-bar", FooterBar);