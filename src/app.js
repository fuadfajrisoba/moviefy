import "regenerator-runtime";
import 'bootstrap/dist/css/bootstrap.min.css';
import "bootstrap/dist/js/bootstrap.min.js"
import "./styles/style.css";

import "./script/component/menu-bar.js";
import "./script/component/search-bar.js";
import "./script/component/menu-bottom.js";
import "./script/component/footer-bar.js";
import main from "./script/view/main.js";
document.addEventListener("DOMContentLoaded", main);